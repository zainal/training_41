# ==============STRING=======================
# =====================================================================================================
# A String object holds and manipulates an arbitrary sequence of bytes, 
# typically representing characters. String objects may be created using String::new or as literals.
# Because of aliasing issues, users of strings should be aware of the methods that modify the 
# contents of a String object. Typically, methods with names ending in “!” modify their receiver, while 
# those without a “!” return a new String. However, there are exceptions, such as String#[]=.

 name = "Zainal"
  a = 10
  b = 12
  puts "My name is #{name}"       # my name is Zainal
  puts "My Age is #{a} + #{b} = #{ a + b }" # 7 + 3 = 22

# str + other_str menggabungkan other_str ke str, menghasilkan object baru hasil gabungan kedua string tersebut.
puts "str + other_str"
puts "ini adalah sebuah string", 'ini juga'

# str << other_str menggabungkan other_str ke str, mengubah object str (tidak menghasilkan object baru).
puts "str << other_str"
puts "Hello from " + "self.to_s"

# str <=> other_str membandingkan dua string, menghasilkan -1 jika str lebih kecil dari other_str, 1 jika str lebih besar dari str, dan 0 jika str sama dengan other_str
puts "str <=> other_str"
puts "aku" <=> "ak"
puts "aku" <=> "aku"
puts "aku" <=> "akui"

# str == other_str membandingkan kesamaan dua string, menghasilkan true jika sama, false jika tidak sama.
puts "str == other_str"
puts "aku" == "aku" #true
puts "aku" == "au" #false

# str.capitalize mengubah huruf pertama menjadi huruf kapital, menghasilkan object baru.
puts "str.capitalize"
a =  "zainal mustofa"
puts a.capitalize

# str.capitalize! mengubah huruf pertama menjadi huruf kapital dengan mengubah object yang ada.
puts "str.capitalize!"
s =  "zainal mustofa" 
puts s.capitalize!

# str.upcase mengubah semua huruf menjadi huruf kapital, menghasilkan object baru.
puts "str.upcase"
x = "41studio"
puts x.upcase

# str.upcase! mengubah semua huruf menjadi huruf kapital dengan mengubah object yang ada.
puts "str.upcase!"
z = "bandung"
puts z.upcase!

# str.length menghasilkan panjang string.
puts "str.length"
b = "aku dia dan semuanya adalah sama "
puts b.length

# str.strip menghapus whitespace yang ada di depan dan di belakang string.
puts "str.strip"
"    hello    ".strip