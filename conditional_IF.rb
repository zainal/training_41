# Conditional statement mengecek apakah suatu ekspresi bernilai
# benar atau salah dan menjalankan logic sesuai dengan hasil dari ekspresi tersebut.

puts "IF statement"
if 3 > 2
    puts "Tiga lebih besar dari dua"
end

puts "================="
puts "operator ! kebalikan atau tidak"
x = 2
y = 3
if !(x > y)
	puts "X tidak lebih besar dari Y"
end

puts "================="
puts "operator unless "

x = 4
y = 3
unless (x > y)
	puts "X tidak lebih besar dari Y"
else		
		puts "Y lebih besar dari X"
end

puts "================="
ruby = "great"
programming = "fun"
puts "boolean in ruby Conditional"
  if ruby == "great" && programming == "fun"
    puts "I love programming with ruby"
  end

puts "================="
puts "Conditional menggunakan statement singkat"
puts "love programming ruby" if ruby == "great" or programming == "fun"

puts "================="
puts "Conditional else if"
name = ""
if name== "zainal"
	puts "selamat datang #{name}"
elsif name == ""
	puts "siapa nama anda?"
else
	puts "anda bukan siapa-siapa #{name}"
end
