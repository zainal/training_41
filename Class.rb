class Hello
  def initialize( name ) 
    @name = name
  end

  def hello_txt
    puts "Hello, " + @name + "!"
  end
end

hi = Hello.new("World")
hi.hello_txt

# Method initialize pada Ruby merupakan constructor, yang otomatis akan dieksekusi pertama kali 
# setelah objek kelas tersebut diciptakan. Pada contoh di atas, method initialize akan 
# mendefinisikan sebuah instance variable @name dan meng-assign-nya dengan argumen yang dimasukkan.