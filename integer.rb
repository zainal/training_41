# integer adalah kelas basic 

#to_s
puts "to_s"
a = 12345
puts a.to_s #"12345"

#downto(limit) {|i| block } → self
puts "downto"
5.downto(1) {|i| print i, " ,"}
puts "enter \n"
#result 5,4,3,2,1, => 5

#next menambahkan angka selanjutnya
puts "next"
puts 1.next

#pred mengembalikan ke -1
puts "pred"
puts 1.pred

#round menambahkan angka desimal dengan default 0
puts "round"
puts 15.round(-1) 
puts 15.round(1)
puts 15.round

#times{|i| block} 
puts "times"
5.times do |i|
  print i, " "
end
puts "\n"