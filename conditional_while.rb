#while can work if condition is have value true
i = 0
  until i < 10
    puts "hello"
    i += 1
end

puts "======================"
#until can work if condition which will execute a block of code to the value conditionalnya be true
j = 10
  until j >= 10
    puts "hello"
    j += 1
  end
puts "======================"
